				###########################################################
				###			MIS ATAJOS DE BSPWM		###
				###########################################################

#TERMINAL
SUPER + ENTER	st

#DMENU Y MIS SCRIPTS
SUPER + d		menu
ALT + p			Lista de pdf(zathura)
SHIFT + ALT + P 	Lista de pdf(evince)
SHIFT + SUPER + v	Lista de mp4
SUPER + ALT + ESC	Shutdown, Reboot, Refresh, y Logout
SUPER + ALT + l 	Idioma del teclado (Ing y Esp)

#APLICACIONES

ALT + a 		Ardour
ALT + b			Virtualbox
ALT + g			Gimp
ALT + k			Kdenlive
ALT + r 		Ranger
ALT + t			Thunar
ALT + w  		Brave
ALT + SHIFT + m		Mousepad
ALT + SHIFT + q   	qjackctl

#CERRAR PAGINAS
SUPER + q		Cerrar node

#CAMBIAR EL ESCRITORIO VIRTUAL
SUPER + numero(0-9)

#SELECTIONA NUEVA O VIEJA NODE DE SU HISTORIA
SUPER + i		Nueva
SUPER + o		Vieja

#SELECCIONA EL ULTIMO/PROXIMO NODE
SUPER + (SHIFT) + n

#SELECCIONA EL NODE EN UN DIRECCION ESPECIFICA
SUPER + {h,j,k,l}

#SELECCIONA EL NODE MAS GRANDE
SUPER + g

#SELECIONA/DESELECIONA VARIOS NODES
SUPER + {b,v}

SELECIONA EL ULTIMO NODE EN TU HISTORIA
SHIFT + ALT {u,t}

##LAYOUTS##
SUPER + m 		cambia de mosaico a monacle
SUPER + {f,w,e}		{fullscreen, flotante,tiled}
CTL + ALT + {h,j,k,l}	Alternar los nodes

#CAMBIAR EL TAMANO DE LOS NODES
ALT + SUPER + (SHIFT) + {left,down,up,right}

#MOVER VENTANA FLOTANTE
CTL + ALT + (SHIFT) {Left,Down,Up,Right}

#EXPANDIR/CONTRAER LOS GAPS
SUPER + ALT {equal,minus}
SUPER + equal
SUPER + O

#RATON PARA CAMBIAR TOMANO DE LOS NODES
SUPER + CLIC DERECHA 
